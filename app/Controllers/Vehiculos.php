<?php

namespace App\Controllers;

use App\Models\VehiculosModel;

class Vehiculos extends BaseController
{
    private $vehiculoModel;

    public function __construct()
    {
        $this->vehiculoModel = new VehiculosModel();
    }

    public function index()
    {
        // $db = \Config\Database:: connect();

        // $query = $db->query("SELECT id, nombre FROM marcas");
        // $resultado = $query->getResult();

        $vehiculoModel = new VehiculosModel();
        $resultado = $vehiculoModel->findAll();

        $data = ['titulo' => 'Mapa', 'marcas' => $resultado];
        return view('vehiculos/index', $data);
    }

    public function show($id)
    {
        $vehiculoModel = new VehiculosModel();
        $marca = $vehiculoModel->find($id);

        $data = [
            'titulo' => 'Mapa',
            'marca' => $marca
        ];

        return view("vehiculos/show", $data);
    }

    public function transaccion()
    {
        $data = [ 
            'nombre' => 'ONIX'
        ];

        echo $this->vehiculoModel->purgeDeleted();
    }

    public function cat($categoria, $id)
    {
        return "<h2>Categoría: $categoria <br> Producto: $id</h2>";
    }
}