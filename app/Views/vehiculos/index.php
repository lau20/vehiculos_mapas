<?php echo $this->extend('plantilla/layout'); ?>

<?php echo $this->section('contenido'); ?>
<div style="text-align: center;">
    <h2>Mapa de Marinilla</h2>
    <style>
        @import url("https://unpkg.com/leaflet@1.9.4/dist/leaflet.css");

        #map {
            margin: 0 auto;
            width:  50%;
            min-height: 400px;
            max-height: 500px;
            height: 100%;
        }
    </style>
    <div id="map"></div>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
        integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
        crossorigin="">
    </script>
    <script>
        var map = L.map('map').setView([6.1730953,-75.3481333], 15);

        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        // Geolocalización
        // map.locate({setView: true, maxZoom: 16});

        var marker = L.marker([6.1749052,-75.340011]).addTo(map);

        var circle = L.circle([6.1736563,-75.3372307], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 200
        }).addTo(map);

        var polygon = L.polygon([
            [6.1736243,-75.3362008],
            [6.1777096,-75.3315015],
            [6.177027,-75.3352244]
        ]).addTo(map);
    </script>
</div>

<?php echo $this->endSection(); ?>

