<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Vehiculos::index');
$routes->get('/vehiculos', 'Vehiculos::index'); 
$routes->get('/vehiculos/(:num)', 'Vehiculos::show/$1'); 
$routes->get('/vehiculos/(:alpha)/(:num)','Vehiculos::cat/$2/$1');
$routes->get('/vehiculos/transaccion','Vehiculos::transaccion');


//Para llamar directamente a la vista desde la ruta 
$routes->view('vehiculosList/(:alpha)', 'lista_vehiculos');

//Definición de grupos de rutas(Para que un grupo pueda heredar un filtro y no definir uno a uno en cada ruta)
$routes->group('admin', static function($routes){
    $routes->get('/vehiculos', 'Admin\Vehiculos::index');
});